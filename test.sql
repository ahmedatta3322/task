
(
select country.name,count(distinct employee.id) 
from country
inner join working_center on country.id = working_center.id
left join employee on country.id = employee.country_id
right join employee_working_center on employee_working_center.working_center_id = working_center.id
where employee_working_center.start_date <= curdate() - interval 1 year 
group by employee.country_id 
order by count(distinct employee.id) desc
)
