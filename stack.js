// I will use console log to show returned value
function stack( stackOperation, stackValue ) {
	 var stackHolder = {
	// We will remove count and use arr length for mor dynamic code
	//count : 3 ,
	storage : [
	  1,
	  '{id: 1,value: "obj"}',
	  "stringHolder",
	  46
	]
  };
  var push = function(value) {
	// to add the value to the end of the arr we replace count by length
	stackHolder.storage[stackHolder.storage.length] = value;
	console.log(stackHolder)
	return stackHolder.storage;
  }
  
  var pop = function() {
	  // we will use length instead of count
    if (stackHolder.storage.length === 0) {
	  return [];
	}
    var poppedItem = stackHolder.storage[stackHolder.storage.length-1];
	// we will use splice instead of delete to acoid empty items 
	stackHolder.storage.splice(-1,1)
	console.log(stackHolder)
    return poppedItem;
  }
  
  var peek = function() {
	//fix it by length 
	console.log(stackHolder.storage[stackHolder.storage.length-1])
	return [stackHolder.storage[stackHolder.storage.length-1]];
  }
  
  var swap = function() {
	[stackHolder.storage[stackHolder.storage.length-1] , stackHolder.storage[stackHolder.storage.length-2]] = [stackHolder.storage[stackHolder.storage.length-2] , stackHolder.storage[stackHolder.storage.length-1]]
	console.log(stackHolder)
	return stackHolder.storage;
  }

  switch(stackOperation) {
	case 'push':
	  push(stackValue);
	break;
	case 'pop':
	  pop();
	break;
	case 'swap':
	  swap();
	break;
	case 'peek':
	  peek();
	break;
	default:
	  return stackHolder.storage;
  }
}
stack("push")