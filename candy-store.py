#I'm using print for debugging and viwing purpes
def buying_candy( amount_of_money ) :
  #check edge cases if money = 0 or 1 
  if amount_of_money == 1 :
    print(1)
    return 1
  elif amount_of_money < 1 : 
    print(0)
    return 0 
  dp = {
    0: 1,
    1: 2
  }
  # set x = 1 to avoid - 1 issue
  x = 1
  #print(dp)
  while x < amount_of_money:
    #fix the summation 
    dp[x+1] = dp[x] + dp[x - 1]
    x = x + 1
  #return the value by index
  return dp[amount_of_money - 1]

buying_candy(7)